<?php include "includes/admin_header.php"; ?>
    <div id="wrapper">

        <!-- Navigation -->
<?php include "includes/admin_navigation.php"; ?>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Welcome to Admin
                            <small>Subheading</small>
                        </h1>
                        <div class="col-xs-6">
                        <?php 
                      insert_categories();      
                            ?>
                        
                        
                        
<?php 
   if(isset($_GET['edit'])){
//       $cat_id=$_GET['edit'];           
        include "includes/update_categories.php";
   }else{
?>                    
                        
        <form action="" method="post">
          <div class="form-group">
             <label for="cat-title">Add Category</label>
              <input type="text" class="form-control" name="cat_title">
          </div>
           <div class="form-group">
              <input class="btn btn-primary" type="submit" name="submit" value="Add Category">
          </div>


        </form>
<?php
   }
?>
                              
                    
            </div>               
                            <div class="col-xs-6">
                            
    <table class="table table-bordered table-hover text-center">   
        <thead>
            <tr>
                <th>Id</th>
                <th>Category Title</th>
                <th colspan="2">Actions</th>
            </tr>
        </thead>
        <tbody>
    <?php
                     $query="select * from categories";
                    $select_categories=mysqli_query($connection,$query);
                    
                    while($row = mysqli_fetch_assoc($select_categories)){
                        $cat_id = $row['cat_id'];
                       $cat_title = $row['cat_title'];
                        
                        ?>
                        
                        <tr>
                            <td><?= $cat_id ?></td>
                            <td><?= $cat_title ?></td>
                            <td><a class="btn btn-success" href='categories.php?edit=<?= $cat_id ?>'>Edit</a></td>
                             <td><a class="btn btn-danger" href='categories.php?delete=<?= $cat_id ?>'>Delete</a></td>
                        </tr>
                    <?php
                        }
                    ?>
                    
        <?php      
              
            delete_categories();
            
  ?>

      

        </tbody>
    </table>

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
<?php include "includes/admin_footer.php"; ?>
